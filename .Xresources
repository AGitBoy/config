! vim: set ft=xdefaults

! dark color definitions {{{
#define DarkAnsi_0_Color #000000
#define DarkAnsi_1_Color #bb0000
#define DarkAnsi_10_Color #55ff55
#define DarkAnsi_11_Color #ffff55
#define DarkAnsi_12_Color #5555ff
#define DarkAnsi_13_Color #ff55ff
#define DarkAnsi_14_Color #55ffff
#define DarkAnsi_15_Color #ffffff
#define DarkAnsi_2_Color #00bb00
#define DarkAnsi_3_Color #bbbb00
#define DarkAnsi_4_Color #0000bb
#define DarkAnsi_5_Color #bb00bb
#define DarkAnsi_6_Color #00bbbb
#define DarkAnsi_7_Color #bbbbbb
#define DarkAnsi_8_Color #555555
#define DarkAnsi_9_Color #ff5555
#define DarkBackground_Color #000000
#define DarkBadge_Color #ff0000
#define DarkBold_Color #ffffff
#define DarkCursor_Color #bbbbbb
#define DarkCursor_Guide_Color #a6e8ff
#define DarkCursor_Text_Color #ffffff
#define DarkForeground_Color #bbbbbb
#define DarkLink_Color #0645ad
#define DarkSelected_Text_Color #000000
#define DarkSelection_Color #b5d5ff
! }}}

! xterm settings {{{
XTerm*.color0:      DarkAnsi_0_Color
XTerm*.color1:      DarkAnsi_1_Color
XTerm*.color2:      DarkAnsi_2_Color
XTerm*.color3:      DarkAnsi_3_Color
XTerm*.color4:      DarkAnsi_4_Color
XTerm*.color5:      DarkAnsi_5_Color
XTerm*.color6:      DarkAnsi_6_Color
XTerm*.color7:      DarkAnsi_7_Color
XTerm*.color8:      DarkAnsi_8_Color
XTerm*.color9:      DarkAnsi_9_Color
XTerm*.color10:     DarkAnsi_10_Color
XTerm*.color11:     DarkAnsi_11_Color
XTerm*.color12:     DarkAnsi_12_Color
XTerm*.color13:     DarkAnsi_13_Color
XTerm*.color14:     DarkAnsi_14_Color
XTerm*.color15:     DarkAnsi_15_Color
XTerm*.colorBD:     DarkBold_Color
XTerm*.foreground:  DarkForeground_Color
XTerm*.background:  DarkBackground_Color
XTerm*.cursorColor: DarkCursor_Color
XTerm*.decTerminalID: vt340
XTerm*.numColorRegisters: 256
XTerm*.termName: xterm-256color
XTerm*.faceName: DejaVu Sans Mono
XTerm*.faceSize: 9
XTerm*.ScrollBar: false
! }}}

! prevent emacs from appearing white initially {{{
Emacs*.color0:      DarkAnsi_0_Color
Emacs*.color1:      DarkAnsi_1_Color
Emacs*.color2:      DarkAnsi_2_Color
Emacs*.color3:      DarkAnsi_3_Color
Emacs*.color4:      DarkAnsi_4_Color
Emacs*.color5:      DarkAnsi_5_Color
Emacs*.color6:      DarkAnsi_6_Color
Emacs*.color7:      DarkAnsi_7_Color
Emacs*.color8:      DarkAnsi_8_Color
Emacs*.color9:      DarkAnsi_9_Color
Emacs*.color10:     DarkAnsi_10_Color
Emacs*.color11:     DarkAnsi_11_Color
Emacs*.color12:     DarkAnsi_12_Color
Emacs*.color13:     DarkAnsi_13_Color
Emacs*.color14:     DarkAnsi_14_Color
Emacs*.color15:     DarkAnsi_15_Color
Emacs*.colorBD:     DarkBold_Color
Emacs*.foreground:  DarkForeground_Color
Emacs*.background:  DarkBackground_Color
Emacs*.cursorColor: DarkCursor_Color
! }}}

! st settings {{{
st.color0:      DarkAnsi_0_Color
st.color1:      DarkAnsi_1_Color
st.color2:      DarkAnsi_2_Color
st.color3:      DarkAnsi_3_Color
st.color4:      DarkAnsi_4_Color
st.color5:      DarkAnsi_5_Color
st.color6:      DarkAnsi_6_Color
st.color7:      DarkAnsi_7_Color
st.color8:      DarkAnsi_8_Color
st.color9:      DarkAnsi_9_Color
st.color10:     DarkAnsi_10_Color
st.color11:     DarkAnsi_11_Color
st.color12:     DarkAnsi_12_Color
st.color13:     DarkAnsi_13_Color
st.color14:     DarkAnsi_14_Color
st.color15:     DarkAnsi_15_Color
st.foreground:  DarkForeground_Color
st.background:  DarkBackground_Color
st.cursorColor: DarkCursor_Color
st.alpha: 255
st.font: Fira Code:pixelsize=12:antialias=true:autohint=true;
! }}}
