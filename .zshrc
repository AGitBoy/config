# -*- mode: sh; origami-fold-style: triple-braces -*-

# add /usr/share/zsh/site-functions to fpath
fpath=( /usr/share/zsh/site-functions "${fpath[@]}" )

if [[ ! -d ~/.zplug ]]; then
	curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
fi

[[ -f ~/.shcommon ]] && . ~/.shcommon
[[ -f ~/.profile ]] && . ~/.profile

source ~/.zplug/init.zsh
	zplug "plugins/pip",               from:oh-my-zsh
	zplug "plugins/git",               from:oh-my-zsh
	zplug "plugins/golang",	           from:oh-my-zsh
	zplug "plugins/rust",              from:oh-my-zsh
	zplug "plugins/command-not-found", from:oh-my-zsh
	zplug "plugins/dnf",               from:oh-my-zsh

	zplug "cperrin88/Snappy_zsh_autocompletion"
	
	zplug "zsh-users/zsh-completions"
	zplug "hlissner/zsh-autopair"

	if [[ "$COLORS" -gt 8 ]]; then
		zplug "dracula/zsh", as:theme
	fi

	# yadm completion
	fpath=("$ZPLUG_HOME/bin" $fpath)
	zplug "TheLocehiliosan/yadm", \
		rename-to:_yadm, \
		use:"completion/yadm.zsh_completion", \
		as:command, defer:2

	# Fish-like features
	if [[ "$COLORS" -gt 8 ]]; then
		zplug "zsh-users/zsh-autosuggestions"
		zplug "AGitBoy/fast-syntax-highlighting"
		zplug "zsh-users/zsh-history-substring-search"
	fi

	if ! zplug check --verbose; then
		printf "Install? [y/N]: "
		if read -q; then
			echo; zplug install
		fi
	fi
zplug load

# Zsh extended filename globbing
setopt extended_glob

# Prevent beep
#
# Note: I would recommend running `rmmod pcspkr` and blacklisting
# the pcspkr kernel module to completely disable the beep,
# if it is enabled in your distro by default
setopt no_beep

# interactive commenting
setopt interactivecomments

# spelling correction
setopt correct

# don't clobber output without trailing newline
setopt promptcr

# automatic cd
setopt autocd

# Load some modules
zmodload -i zsh/parameter
zmodload -i zsh/complist
zmodload -i zsh/deltochar
zmodload -i zsh/mathfunc

autoload -Uz zcalc

# mime type stuff
zstyle :mime: mailcap ~/lib/zmailcap ~/.mailcap /usr/local/etc/mailcap /etc/mailcap
autoload zsh-mime-setup && zsh-mime-setup

# History settings
SAVEHIST=2000
HISTSIZE=2000
HISTFILE=~/.zsh_history

# Load as needed
zmodload -a zsh/stat zstat
zmodload -a zsh/zpty zpty
zmodload -ap zsh/mapfile mapfile

# misc completions
for program in adpt pwr lsgrp; do
	compdef _gnu_generic $program
done

# colormake
if exists colormake; then
	compdef _make colormake
fi

# completions for pkg script
if exists dnf; then
	compdef _dnf pkg
elif exists yum; then
	compdef _yum pkg
elif exists apt; then
	compdef _apt pkg
elif exists apt-get; then
	compdef _apt-get pkg
elif exists pacman; then
	compdef _pacman pkg 
elif exists emerge; then
	compdef _emerge pkg
elif exists zypper; then
	compdef _zypper pkg
elif [[ -f /usr/sbin/pkg ]]; then
	compdef _bsd-pkg pkg
elif exists pkcon; then
	compdef _pkcon pkg
fi

# global aliases
alias -g ...='../..'
alias -g ....='../../..'
alias -g BG='2>&1 1>/dev/null & disown'
alias -g G="|grep"
alias -g H="|head"
alias -g P="|pager"
alias -g PP="|& pager"
alias -g N="&>/dev/null"
alias -g T="|tail"
alias -g V="|& vim -"

# menu for completion
zstyle ':completion:*' menu select

if [[ "$COLORS" -ge 8 ]]; then
	zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

	# sort completions into groups
	zstyle ':completion:*:descriptions' format '%F{red}Completing %B%d%b%f' 
	zstyle ':completion:*:matches' group 'yes'
	zstyle ':completion:*' group-name ''
fi

# completion caches
[[ ! -d ~/.cache/zshcomp ]] && mkdir -p ~/.cache/zshcomp
zstyle ':completion:*' use-cache yes
zstyle ':completion:*' cache-path "$HOME/.cache/zshcomp"



# Keybindings
if [[ "$TERM" != "eterm-color" ]]; then
	# vi keybindings
	bindkey -v
	bindkey '' vi-beginning-of-line
	bindkey '' vi-end-of-line

	# vi keys completion menu
	bindkey -M menuselect 'h' vi-backward-char
	bindkey -M menuselect 'k' vi-up-line-or-history
	bindkey -M menuselect 'l' vi-forward-char
	bindkey -M menuselect 'j' vi-down-line-or-history

	# history navigation
	if exists history-substring-search-up && \
		 exists history-substring-search-down; then
		bindkey 'OA' history-substring-search-up
		bindkey 'OB' history-substring-search-down

		bindkey '[A' history-substring-search-up
		bindkey '[B' history-substring-search-down
		
		bindkey -M vicmd 'k' history-substring-search-up
		bindkey -M vicmd 'j' history-substring-search-down

		bindkey '' history-substring-search-up
		bindkey '' history-substring-search-down
	fi
else
	bindkey -e
fi

# percol history search
if exists percol && [[ "$COLORS" -ge 8 ]]; then
		function percol_select_history() {
				local tac
				exists gtac && tac="gtac" || { exists tac && tac="tac" || { tac="tail -r" } }
				BUFFER=$(fc -l -n 1 | eval $tac | awk '!seen[$0]++' | percol --query "$LBUFFER")
				CURSOR=$#BUFFER
				zle -R -c
		}

		zle -N percol_select_history
		bindkey '^R' percol_select_history
fi

# 9term-like completion
bindkey '[2~' menu-complete

fancy-ctrl-z () {
	if [[ $#BUFFER -eq 0 ]]; then
		BUFFER="fg"
		zle accept-line
	else
		zle push-input
		zle clear-screen
	fi
}
 
zle -N fancy-ctrl-z
bindkey '' fancy-ctrl-z

if [[ "$COLORS" -gt 8 ]]; then
	PROMPT_EOL_MARK="⏎"
fi

case $TERM in
	st*|screen*|tmux*)
		DRACULA_ARROW_ICON='→'
		;;
	*)
		DRACULA_ARROW_ICON='➜'
		;;
esac

if [[ -n "$KONSOLE_VERSION" ]]; then
	ZSH_THEME_GIT_PROMPT_CLEAN=") %F{green}%B✔ "
fi

if ! [[ "$TERM" = eterm-color ]]; then
	if [[ "$COLORS" -lt 8 ]]; then
		PROMPT='%n@%m:%~%(!.#.$) '
	elif [[ "$COLORS" -eq 8 ]]; then
		PROMPT='%F{green}%B%n@%m%f%b:%F{blue}%B%~%f%b%(!.#.$) '
	fi
else
	PROMPT='%F{green}%B%n@%m%f%b:%F{blue}%B%~%f%b%(!.#.$) '
fi

# set terminal title
if ! [ "$TERM" = eterm-color ]; then
	function precmd() {
			echo -ne "\033]0;$(print -P '%n@%m:%~')\007"
	}
fi
