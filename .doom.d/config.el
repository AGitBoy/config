;;; .doom.d/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Aidan Williams"
      user-mail-address "aidanwillie0317@protonmail.com"

      doom-font (font-spec :family "JetBrains Mono" :size 10)
      doom-theme 'doom-dracula
      display-line-numbers-type 'relative
      centaur-tabs-set-bar 'left
      treemacs-width 32

      projectile-project-search-path '("~/src/")
      org-agenda-files '("~/org")
      org-latex-pdf-process '("latexmk -pdf %f"))

;; emms setup
(use-package emms
  :init
  (require 'emms-setup)
  (require 'emms-info-libtag)
  :config
  (emms-all)
  (emms-default-players)
  (setq emms-source-file-default-directory "~/music/")
  (setq emms-info-asynchronously t)
  (setq emms-info-functions '(emms-info-libtag)))

;; support for Jetbrains Mono ligatures
;; FIXME: broken as of now. See https://github.com/mickeynp/ligature.el/issues/10
;; (use-package ligature
;;   :hook (prog-mode . ligature-mode)
;;   :config
;;   (ligature-set-ligatures 'prog-mode '("-->" "//" "/**" "/*" "*/" "<!--" ":="
;;                                        "->>" "<<-" "->" "<-" "<=>" "==" "!="
;;                                        "<=" ">=" "=:=" "!==" "&&" "&&&" "||"
;;                                        "..." ".." "///" "===" "++" "--" "=>"
;;                                        "|>" "<|" "||>" "<||" "|||>" "<|||::=" "|]"
;;                                        "[|" "|}" "{|" "[<" ">]" ":?>" ":?"
;;                                        "/=" "[||]" "!!" "?:" "?." "::" "+++"
;;                                        "??" "##" "###" "####" ":::" ".?" "?="
;;                                        "=!=" "<|>" "<:" ":<" ":>" ">:" "<>"
;;                                        "***" ";;" "/==" ".=" ".-" "__" "=/="
;;                                        "<-<" "<<<" ">>>" "<=<" "<<=" "<==" "<==>"
;;                                        "==>" "=>>" ">=>" ">>=" ">>-" ">-" "<~>"
;;                                        "-<" "-<<" "<<" "---" "<-|" "<=|" "\\\\"
;;                                        "\\/" "|=>" "|->" "<~~" "<~" "~~" "~~>"
;;                                        "~>" "<$>" "<$" "$>" "<+>" "<+" "+>"
;;                                        "<*>" "<*" "*>" "</" "</>" "/>" "<->"
;;                                        "..<" "~=" "~-" "-~" "~@" "^=" "-|"
;;                                        "_|_" "|-" "||-" "|=" "||=" "#{" "#["
;;                                        "]#" "#(" "#?" "#_" "#_(" "#:" "#!"
;;                                        "#=")))
