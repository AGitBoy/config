#!/bin/sh
# new york times from terminal

MAX_CACHE_ARTICLES=15

if [ $# -lt 1 ]; then
    >&2 echo "usage: nyt [URL]"
    exit 1
fi

mkdir -p ~/.cache/nyt
if [ ! -d ~/.cache/nyt ]; then
    >&2 echo "error creating directory $HOME/.cache/nyt"
    exit 1
fi

cache_file="$HOME/.cache/nyt/$(echo -n "$1" | md5sum | cut -d ' ' -f1)"

if [ -f "$cache_file" ]; then
    exec cat "$cache_file"
else
    if [ $(ls ~/.cache/nyt | wc -l) -ge $MAX_CACHE_ARTICLES ]; then
        rm -f "$(ls -t ~/.cache/nyt | tail -n1)"
    fi
fi

# strip navigation at top
stripnav() {
    awk '/^[A-Za-z0-9]/,EOF { print $0 }'
}

# strip index at bottom
stripind() {
    awk '/^Site Index$/ { exit }; { print }'
}

# an exceedingly long (and bad) regex to strip other elements
stripmisc() {
    grep -v '\((BUTTON)\|^[[:space:]]*Advertisement$\|^[[:space:]]*Video$\|^[[:space:]]*Video player loading$\|^[[:space:]]*Continue reading the main story$\|[*+]$\)'
}

# strip trailing blanks
stripblanks() {
    sed -e :a -e '/^\n*$/{$d;N;};/\n$/ba'
}

lynx -nolist -dump "$1" | stripnav | stripind | stripmisc | stripblanks | tee "$cache_file"

if ! [ -s "$cache_file" ]; then
    rm -f "$cache_file"
fi
